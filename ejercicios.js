var fecha = new Date()

/* var arregloNumeros = [1,2,4,5,7,8,3,543,32,34,323,665,476,3,7,78]
Math.min(...arregloNumeros)


function sumar(a,b,c){
    console.log('a', a)
    console.log('b', b)
    console.log('c', c)
}
var encapsulamiento = [1,['a','b'],{nombre: 'Miguel'}]
sumar(...encapsulamiento) */

console.log(fecha)
//Sacar el año
console.log(fecha.getFullYear())
var fechaNacimiento = new Date('1996-10-10') 
console.log(fecha.getFullYear()-1996)
var edad = fecha.getFullYear()-fechaNacimiento.getFullYear()
console.log(edad)

//Sacar el mes 
console.log(fecha.getMonth())
console.log(fechaNacimiento.getMonth())

//Sacar el dìa
//Saca el día de la semana...
console.log(fecha.getDay())
console.log(fecha.getDate())

console.log(fecha.toTimeString())
console.log(fecha.toString())
console.log(fecha.toISOString())

var numero = 4

console.log(typeof numero)
console.log(typeof parseInt(numero))
console.log(typeof parseFloat(numero))

