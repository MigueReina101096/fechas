const moment = require('moment');
const random = require('random-js')();

//Crear un arreglo con un atributo nombre e ingresar una fecha randomica
//Rango de fechas randomicas 1930-2018
//Calcular edad
//Filtren en menores de edad, adulto, tercera edad 

const arregloUsuarios = [
    { nombre: 'Miguel' },
    { nombre: 'Pedro' },
    { nombre: 'Jose' },
    { nombre: 'Cristian' },
    { nombre: 'Oscar' },
    { nombre: 'Andres' },
    { nombre: 'Paola' },
    { nombre: 'Ricardo' },
    { nombre: 'Carlos' },
    { nombre: 'Adrian' }
];

const fecha = moment();
const anioActual = fecha.year();
function generarFecha() {
    const anioRandom = random.integer(1930, 2018).toString();
    const fechaNacimiento = moment(anioRandom + '-01-01').format('L');
    return fechaNacimiento;
}


arregloUsuarios.map((valor) => {
    /* const anioRandom = random.integer(1930,2018).toString();
    const fechaNacimiento = moment(anioRandom + '-01-01').format('L'); */
    valor.fechaNacimientoUsuario = generarFecha();
    valor.edadUsuario = anioActual - 2018;
})
console.log(arregloUsuarios);

const usuariosNiños = arregloUsuarios.reduce((acumulador, valor) => {
    validadcion = valor.edadUsuario < 18
    if (validadcion) {
        acumulador.push(valor)
    }
    return acumulador;
}, []);
console.log('Arreglo niños ', usuariosNiños);

const usuariosMayores = arregloUsuarios.reduce((acumulador, valor) => {
    validadcion = valor.edadUsuario > 55
    if (validadcion) {
        acumulador.push(valor)
    }
    return acumulador;
}, []);
console.log('Arreglo mayores ', usuariosMayores);

var usuariosAdulto = arregloUsuarios.reduce((acumulador, valor) => {
    validadcion = valor.edadUsuario > 18 && valor.edadUsuario < 55
    if (validadcion) {
        acumulador.push(valor)
    }
    return acumulador;
}, []);
console.log('Arreglo adultos ', usuariosAdulto);